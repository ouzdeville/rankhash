#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>

#include "params.h"
#include "matrix.h"
#include "pi.h"

//extern const unsigned char Pi[2048*1024];

static binmat_t H;
void show(const unsigned char * in, int taille){
	int i;
	for (i = 0; i < taille; i++)
		{
		    //if (i > 0) printf(":");
		    printf("%.2X", in[i]);
		}
	printf("\n");
}
/**
 * in un vecteur de longueur s*n bits , avec W< min (s,n)
 */
void expand(const unsigned char * in, unsigned char * out){

    //A.rwdcnt = (1 + (coln - 1) / BITS_PER_LONG); le nombre de long de chaque ligne
	binmat_t B,A;
	B=mat_ini(W,s);
	//memset(B.elem,0,B.alloc_size);
	A = mat_ini(n, W);

	//memset(A.elem,0,A.alloc_size);
	int i=0,j=0;
	gf_t *result = (gf_t *) calloc(s, sizeof(gf_t));
	//printf("test1\n");
	// recupération des nW premiers bits pour A,pour chaque ligne W bits.
	for (i = 0; i < A.rown; i++) {
		memcpy(A.elem[i],in, A.rwdcnt * sizeof(registre));
			in += A.rwdcnt * sizeof (registre);
		}
	//mat_aff(A);
	//printf("test2\n");
	// Mettre Id_w pour le bloc à gauche de B
	for(i=0;i<W; i++){
		mat_set_coeff_to_one(B,i,i);
	}

	//printf("test3\n");
	//Mettre les bits restants sn-wn=n(s-W) de in dans la deuxieme partie de B (s-W)*W
	// pour chaque ligne de B déplacer le pointer
	int k=(1 + (W - 1) / BITS_PER_REGISTER);
	//printf("test4\n");
	for (i = 0; i < B.rown; i++) {
			memcpy(B.elem[i]+k,in, (B.rwdcnt-k) * sizeof(registre));
				in += (B.rwdcnt-k) * sizeof(registre);
			}
	//mat_aff(B);
	//printf("test5\n");
	// M=A*B;
	binmat_t M=mat_mul(A,B);
	//mat_aff(M);
	//printf("test6\n");
	for (i = 0; i < M.rown; ++i) {
	    for (j = 0; j < M.coln; ++j) {
	    	if(mat_coeff(M,i,j)){
	    		result[j]^=1ULL<<i;
	    	}
		}
	}
	/*for (i = 0; i < s; ++i) {
				printf("%llu,",result[i]);
			}*/

	//printf("test7\n");
	memcpy(out,result, sn8);
	//printf("test8\n");
}

void compress(const unsigned char * in, unsigned char * out){


	int i,j;
	registre* res = (registre*) calloc(H.rwdcnt,sizeof(registre));
	unsigned long long int line;


	//mat_aff(H);
	//decoupe in en W part de taille s/W=log(n/W)
	/*for (j = 0; j < sn8; ++j) {
		printf("%d-",in[j]);
	}
	printf("\n");*/
	int sn_W=sn8/W;
	//printf("%d\n",sn_W);
	for (i = 0; i < W; ++i) {
		//k=i*s_W; i(+1)*s_W-1, cursur=s_w/8
		line=0;
		memcpy(&line,in+i*sn_W, sn_W);
		//printf("%llu +++++++++++++++",line);

		//printf("%llu+++++",line%n);
		int log=0;
		while(line){
			line=line >>1;
			log++;
		}
		//printf("%d++",log);
		line=1ULL<<(log/n);
		//printf("%d++",line);
		//printf("%llu+++++\n",i*(n/W)+line);

		for (j = 0; j < H.rwdcnt; ++j) {
			res[j]^=H.elem[(i*(n/W)+line)%n][j];
		}


	}
	memcpy(out,res, H.rwdcnt * sizeof(registre));
}

void update(const unsigned char * in, unsigned char * out){
    // in de taille sn bits

	unsigned char y[sn8];
	//init y
	expand(in,y);


	//printf("test9\n");
	//unsigned char * x=(unsigned char *) calloc(r, sizeof(gf_t));
	//init x;
	//show(y,sn8);
	compress(y,out);


}

int main(int argc, char** argv)
{
	//unsigned char in[sn8];
	unsigned char* in=(unsigned char*) calloc(sn8,sizeof(unsigned char));
	int i=0;
	unsigned char* out=(unsigned char*) calloc(rn8,sizeof(unsigned char));;// Fixer L'IV à FAIRE

	for (i = 0; i < rn8; ++i) {
				out[i]=0xff^i;
			}
	//printf("For the IV:");
	//show(out,rn8);
	if(1){
		H=mat_ini(n,n*r);
		for (i = 0; i < H.rown; ++i) {
				memcpy(H.elem[i],Pi+i*H.rwdcnt * sizeof (registre), H.rwdcnt * sizeof(registre));
				//Pi += H.rwdcnt * sizeof (registre);
		}
		}
	unsigned long long int databytelen = 0;
    // Lire à chaque tour sn8-rn8 // Oublier Padding pour l'instant
	FILE* f = argc > 1 ? fopen(argv[1], "r") : stdin;
	while(1) {

		databytelen += fread(in, 1, sn8-rn8, f);
		if(feof(f))break;


		/*for (i = 0; i < sn8-rn8; ++i) {
			in[i]=0xff^i^k;
		}*/


		/*
		for (i = 0; i < s*n; ++i) {
				printf("%d",(in[i/8]>>(i%8))&1);
			}*/
		memcpy(in+sn8-rn8,out, rn8);
		//show(in,sn8);
		update(in,out);
	}
	fclose(f);
	// padding handling inspired from FSB
	unsigned long long int lastRead = databytelen % (sn8-rn8);

	in[lastRead] = 0x80;// 10000000 au bloc lastread+1
	if (8 >= (sn8-rn8)-lastRead)//il reste assez d'espace pour stocker la taille du fichier 8 octets
	{
		memset(in+lastRead+1, 0, (sn8-rn8)-lastRead-1);//on complete le block par des zéro
		memcpy(in+sn8-rn8,out, rn8);
		//show(in,sn8);
		update(in, out);
		lastRead = 0;
	}
	//préparation du nouveau block ou mise à jour du dernier
	memset(in+lastRead+1, 0, (sn8-rn8)-lastRead-9);
	unsigned int len = databytelen << 3;// databytelen*8
	for (unsigned int i = 1; i <= 8; i++)
	{
		in[(sn8-rn8)-i] = len;
		len >>= 8;
	}
	memcpy(in+sn8-rn8,out, rn8);
	//show(in,sn8);
	update(in, out);
	// output
		printf("%s: ", argc > 1 ? argv[1] : "\n Standard input");
		show(out,rn8);
		/*for (unsigned int j = 0; j < rn8; j++)
			printf("%.2x", out[j]);
		printf("\n");*/

	//printf("test10\n");


	return 1;
}




