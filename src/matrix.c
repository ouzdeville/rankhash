
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "params.h"
#include "matrix.h"



/*********************************************************************************************/
////////////////////////////////////MATRIX Functions///////////////////////////////////////////
/*********************************************************************************************/

//take a look at the MSB-LSB format???
binmat_t mat_ini(int rown, int coln) {
	binmat_t A;
	unsigned int i;
	//A = (binmat_t) malloc(sizeof(struct matrix));
	A.coln = coln;
	A.rown = rown;
	A.rwdcnt = (1 + (coln - 1) / BITS_PER_REGISTER);
	A.alloc_size = rown * A.rwdcnt * sizeof(registre);
	A.elem = (registre **) malloc(rown * sizeof(registre*));
	for (i = 0; i < rown; i++)
		A.elem[i] = (registre*) calloc(A.rwdcnt,
				sizeof(registre));
	return A;
}

/*mat_t matrix_init(int rown, int coln) {
	unsigned int i;
	mat_t A;
	A.coln = coln;
	A.rown = rown;

	A.coeff = (gf_t **) malloc(rown * sizeof(gf_t *));
	for (i = 0; i < rown; i++) {
		A.coeff[i] = (gf_t *) calloc(coln, sizeof(gf_t));
	}

	return A;
}*/

void mat_free(binmat_t A) {
	unsigned int i;
	for (i = 0; i < A.rown; i++) {
		free(A.elem[i]);
	}
	free(A.elem);
}

/*void mat_rowxor(binmat_t A, int a, int b) {
	int i;
	for (i = 0; i < A.rwdcnt; i++) {
		A.elem[a][i] ^= A.elem[b][i];
	}
	//return A;
}*/
/*
binmat_t mat_swapcol(binmat_t A, int a, int b) {
	int i;
	for (i = 0; i < A.rown; i++) {
		if (mat_coeff(A, i, a) != mat_coeff(A, i, b)) {
			mat_change_coeff(A, i, a);
			mat_change_coeff(A, i, b);
		}

	}
	return A;
}*/

//the matrix is reduced from LSB...(from right)


void mat_aff(binmat_t A) {
	int i,k;
	for (i = 0; i < A.rown; i++) {
		for (k = 0; k < A.coln; k++)

		{
			//printf("%d\t", mat.coeff[i][j]);
			printf("%d", mat_coeff(A, i, k));

		}
		printf("\n");
	}

	printf("\n");
}

/*void aff_mat(mat_t mat) {
	printf("Show new Matrix\n");
	int i, j;
	for (i = 0; i < (mat.rown); i++) {
		for (j = 0; j < (mat.coln); j++) {
			printf("%llu-", mat.coeff[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}*/



/*void mat_free_nb(mat_t A) {
	unsigned int i;
	for (i = 0; i < A.rown; i++) {
		free(A.coeff[i]);
	}
	free(A.coeff);
}*/
void aff_bin_mat(binmat_t mat) {
	printf("Show new Bin Matrix\n");
	int i, j;
	for (i = 0; i < (mat.rown); i++) {
		for (j = 0; j < (mat.coln); j++) {
			printf("%d", mat_coeff(mat, i, j));
		}
		printf("\n");
	}
	printf("\n");
}
binmat_t mat_mul(binmat_t A, binmat_t B)
{
  binmat_t C;
  int i,j,k;

  if (A.coln != B.rown)
    exit(0);

  C = mat_ini(A.rown, B.coln);
  //memset(C.elem,0,C.alloc_size);

  for(i=0;i<A.rown;i++)
    for(j=0;j<B.coln;j++)
      for (k = 0; k < A.coln; ++k)
	if (mat_coeff(A,i,k) && mat_coeff(B,k,j))
	  mat_change_coeff(C,i,j);
  //printf("test51\n");
  return C;
}
void produit_vector_matrix(unsigned long* Res,unsigned char* u,binmat_t A){
		int i,j;
		for(i=0; i<A.rown; i++){
			if ((u[i/8]>>(i%8))&1){
				for(j=0; j<A.rwdcnt; j++)	{
				Res[j]^=A.elem[i][j];
				}
			}


		}
}

