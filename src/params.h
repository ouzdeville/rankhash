

#define BITS_TO_BYTES(nb_bits) (((nb_bits) - 1) / 8 + 1)
#define n 32
#define W 8
#define r 8
#define s W*2
#define sr8 BITS_TO_BYTES(s-r)
#define r8 BITS_TO_BYTES(r)
#define s8 BITS_TO_BYTES(s)
#define sn8 BITS_TO_BYTES(s*n)
#define rn8 BITS_TO_BYTES(r*n)
#define W8  BITS_TO_BYTES(W)
#if n==32
typedef unsigned int gf_t;
#else
typedef unsigned long long int gf_t;
#endif
