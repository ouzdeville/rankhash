#include "params.h"
#ifndef _MATRIX_H
#define _MATRIX_H

/*
#ifdef ARCHI_8
 typedef unsigned char registre;
 #define WORDSIZE 1
#elif ARCHI_32
 typedef unsigned int registre;
 #define WORDSIZE 4
#else
 typedef unsigned long long int registre;
 #define WORDSIZE 8
#endif*/


typedef unsigned char registre;// unsignedchar ou  uint16_t ou unsigned long .le plus grand qui divise les paramètres
#define BITS_PER_REGISTER (8* sizeof(registre))//
// number of bytes needed for storing nb_bits bits
#define BITS_TO_BYTES(nb_bits) (((nb_bits) - 1) / 8 + 1)
typedef struct matrix{
   int rown;//number of rows.
   int coln;//number of columns.
   int rwdcnt;//number of words in a row
   int alloc_size;//number of allocated bytes
   registre **elem;//row index.
   }binmat_t;

/*typedef struct Matrix
{
	unsigned int rown;
	unsigned int coln;
	int rwdcnt;//number of words in a row
	gf_t **coeff;
} mat_t;
*/




#define mat_coeff(A, i, j) (((A).elem[(i)][(j) / BITS_PER_REGISTER] >> (j % BITS_PER_REGISTER)) & 1)
#define mat_set_coeff_to_one(A, i, j) ((A).elem[(i)][(j) / BITS_PER_REGISTER] |= (1UL << ((j) % BITS_PER_REGISTER)))
#define mat_change_coeff(A, i, j) ((A).elem[(i)][ (j) / BITS_PER_REGISTER] ^= (1UL << ((j) % BITS_PER_REGISTER)))
#define swaprows(A, i, k) {unsigned char * pt = A.elem[i]; A.elem[i] =A.elem[k]; A.elem[k] = pt;}


binmat_t mat_ini(int rown, int coln);
void mat_free(binmat_t A);
//void  mat_rowxor(binmat_t A,int a, int b);
int* mat_rref(binmat_t A);
void mat_aff(binmat_t A);
//mat_t matrix_init(int rown, int coln);
//void aff_mat(mat_t mat);
//void mat_free_nb(mat_t A);
void aff_bin_mat(binmat_t mat);
binmat_t mat_mul(binmat_t A, binmat_t B);
#endif

